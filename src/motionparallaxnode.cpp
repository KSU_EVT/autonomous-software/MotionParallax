#include <stdio.h>
#include <math.h>
#include "frame.hpp"
#include "point.hpp"

#include "rclcpp/rclcpp.hpp"

#include "message_filters/subscriber.h"
#include "message_filters/time_synchronizer.h"
#include "message_filters/sync_policies/approximate_time.h"
#include "voltron_msgs/msg/cone_bearings.hpp"

#include "voltron_msgs/msg/one_cone.hpp"
#include "voltron_msgs/msg/many_cones.hpp"
#include "nav_msgs/msg/odometry.hpp"

#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"

#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#include <tf2/LinearMath/Quaternion.h>


using std::placeholders::_1;
using std::placeholders::_2;
using namespace message_filters;
using namespace message_filters::sync_policies;

using namespace motion_parallax;

class MotionParallaxSubscriber : public rclcpp::Node
{
public:
  MotionParallaxSubscriber() : Node("MotionParallaxNode")
  {

    rel_bearing_sub_ = std::make_shared<message_filters::Subscriber<voltron_msgs::msg::ConeBearings>>(this, "cone_bearings");
    vectornav_pose = std::make_shared<message_filters::Subscriber<nav_msgs::msg::Odometry>>(this, "odometry/gps");
    const ApproximateTime<voltron_msgs::msg::ConeBearings, nav_msgs::msg::Odometry> policy_((uint32_t)10);
    time_sync_ = std::make_shared<Synchronizer<ApproximateTime<voltron_msgs::msg::ConeBearings, nav_msgs::msg::Odometry>>>(policy_, *rel_bearing_sub_, *vectornav_pose);
    printf("registered callback\n");
    many_cones_pub_ = this->create_publisher<voltron_msgs::msg::ManyCones>("cone_triangulations", 10);
    time_sync_->registerCallback(std::bind(&MotionParallaxSubscriber::bearing_callback, this, _1, _2));
  }

private:
  std::shared_ptr<message_filters::Subscriber<voltron_msgs::msg::ConeBearings>> rel_bearing_sub_;
  std::shared_ptr<message_filters::Subscriber<nav_msgs::msg::Odometry>> vectornav_pose;
  std::shared_ptr<Synchronizer<ApproximateTime<voltron_msgs::msg::ConeBearings, nav_msgs::msg::Odometry>>> time_sync_;
  rclcpp::Publisher<voltron_msgs::msg::ManyCones>::SharedPtr many_cones_pub_;

  Frame *last_frame = nullptr;

  // https://docs.ros2.org/foxy/api/geometry_msgs/msg/PoseWithCovariance.html
  void bearing_callback(const voltron_msgs::msg::ConeBearings::ConstSharedPtr &relative, const nav_msgs::msg::Odometry::ConstSharedPtr &odom_stamped)
  {
    printf("bearing callback was called\n");
    printf("TimeStamp: %d\n", relative->header.stamp.sec);
    printf("Frame ID: %s\n", relative->header.frame_id.c_str());

    auto &pose = odom_stamped->pose.pose;

    printf("got past sahans auto pointer\n");

    tf2::Transform transform;
    tf2::fromMsg(pose, transform);
    tf2::Quaternion q = transform.getRotation();
    tf2::Matrix3x3 m(q);
    double yaw, pitch, roll;
    m.getEulerYPR(yaw, pitch, roll);
    float yaw_angle = static_cast<float>(yaw);

    printf("facing yaw_angle: %f\n", yaw_angle);
    const std::vector<float> &relative_bearings = relative->data;
    std::vector<double> double_bearings(relative_bearings.size());
    for (int i = 0; i < double_bearings.size(); ++i)
    {
      double_bearings[i] = static_cast<double>(relative_bearings[i]);
    }
    geometry_msgs::msg::Quaternion heading_quat = pose.orientation;
    geometry_msgs::msg::Point xyz = pose.position;
    float x = static_cast<float>(xyz.x);
    float y = static_cast<float>(xyz.y);
    float z = static_cast<float>(xyz.z);
    printf("x: %f, y: %f, z: %f\n", x, y, z);

    // // TODO x and y are in wrong reference frame, fix this pls
    Frame *f = new Frame(yaw_angle, x, y, double_bearings, last_frame);

    std::vector<ObjDetection> triangulated_dets = f->triangulations();

    voltron_msgs::msg::ManyCones many_cones;
    many_cones.header.stamp = relative->header.stamp;
    many_cones.header.frame_id = "map"; // TODO parameterize this

    // TODO publish triangulations here!!!
    for (const auto &det : triangulated_dets)
    {
      auto &centroid = det.centroid.value();

      auto &one_cone = many_cones.cones.emplace_back();
      one_cone.x_var = det.x_var;
      one_cone.y_var = det.y_var;
      one_cone.num_samples = det.num_samples;
      one_cone.point.x = centroid.x;
      one_cone.point.y = centroid.y;
      one_cone.point.z = 0.0;
    }

    many_cones_pub_->publish(many_cones);
    last_frame = f;
  }
};

int main(int argc, char **argv)
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<MotionParallaxSubscriber>());
  rclcpp::shutdown();
  return 0;
}
